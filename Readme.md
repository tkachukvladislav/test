Установить и настроить все окружение (слайд с названием “Настройка ПО. Факт-карта.”).
Создать и настроить виртуал хост для тестового проекта. Проверить его работу (запустить с браузера).
Пройти интерактивную обучалку по ГИТу (Основы - первые 3 блока, Удалённые репозитории - 1 блок).
Задача по ГИТу (примеры тут):
создать локально проект,
инициализировать в нем гит,
привязать к проекту удаленный репозиторий (который нужно создать на Gitlab, если такового нет),
создать 3 ветки (`master`, `develop` отколоться от `master`, `feature` отколоться от `develop`) и сразу запушить master в удаленный репозиторий,
в ветке develop добавить в проект Readme.md файл с текстом данного ДЗ и  запушить в удаленный репозиторий,
в ветке feature добавить в проект файл index.php и вывести в нем надпись `Hello World!` (работоспособность можно проверить через встроенные в PHP web-сервер) и запушить в удаленный репозиторий,
замержить локально ветку feature в develop,
запушить ветку develop в удаленный репозиторий и сделать Pull (Merge) Request с develop в master,
ссылка на данный pull request будет считаться результатом выполнения вашей ДЗ (ее нужно прислать мне для проверки).
Попрактиковаться в Линукс в командной строке на таких командах:
Создать текстовый файл: cat > file.txt [enter] ввести текст [enter][ctrl+d] — и файл создан
Создать текстовый файл содержащий данную строку: echo Hello World > file.txt
Просмотреть текстовый файл: cat file.txt
